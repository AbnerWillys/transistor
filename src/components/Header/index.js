import React from 'react'

import Logo from '../../assets/Logo.png'

import styled from 'styled-components';

export default function Header() {
  return (
    <HeaderStyle>
      <img src={Logo} alt="Logo transistor"/>
      <h1>SuperGiantGames</h1>
    </HeaderStyle>
  )
}

const HeaderStyle = styled.div`
  width: 100%;
  height: 109px;
  background: #363636;
  opacity: 1;

  display: flex;
  justify-content: center;
  align-items: center;

  img {
    margin-right: 30px;
  }

  h1 {
    text-align: left;

    font-size: 22.5px;
    font-family: 'Montserrat', sans-serif;
    font-weight: bold;

    text-transform: uppercase;
    letter-spacing: 0.68px;
    color: #FFFFFF;
  }
`;
